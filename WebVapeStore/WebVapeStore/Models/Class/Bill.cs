﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VapeStore.Models
{
    public class Bill
    {
        [Key]
        public string BillId { get; set; }
        public string BillCode { get; set; }
        public DateTime DateBill { get; set; }
        public double TotalPrice { get; set; }

        public virtual User User { get; set; }
        public virtual Customer Customer { get; set; }

    }
}