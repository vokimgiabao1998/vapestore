﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VapeStore.Models
{
    public class Coupon
    {
        [Key]
        public string CouponId { get; set; }
        public string CouponName { get; set; }
        public string CouponCode { get; set; }
    }
}